# Routos

> This repo is clone from [mikrotik website](https://wiki.mikrotik.com/wiki/API_rust)

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
routos = "0.1.0"
```

# License

Routos is distributed under the terms of [GPL v3](LICENSE)
